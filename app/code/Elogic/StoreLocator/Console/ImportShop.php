<?php

namespace Elogic\StoreLocator\Console;

use Elogic\StoreLocator\Model\ShopFactory;
use Elogic\StoreLocator\Model\ShopRepository;
use Magento\Framework\Console\Cli;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportShop extends Command
{
    const INPUT_KEY_FILE_NAME = 'filename';

    private ShopFactory $shopFactory;
    private ShopRepository $shopRepository;

    public function __construct(
        ShopFactory $shopFactory,
        ShopRepository $shopRepository,
        string $name = null
    ) {
        $this->$shopFactory = $shopFactory;
        $this->$shopRepository = $shopRepository;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('elogic:store:import');
        $this->setDescription('Import store from csv file');
        $this->addArgument(
            self::INPUT_KEY_FILE_NAME,
            InputArgument::REQUIRED,
            'CSV filename'
        );

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fileName = $input->getArgument(self::INPUT_KEY_FILE_NAME);

        if(!preg_match("/\.(csv)$/", $fileName))
        {
            $output->writeln("<fg=red>File extension must be .csv !\n</>");
            return Cli::RETURN_FAILURE;
        }
        try {
            $csvFile = file($fileName);

            $lines = [];
            foreach ($csvFile as $line) {
                $lines[] = str_getcsv($line);
            }

            $keyNames = array_shift($lines);
            $data = [];
            foreach ($lines as $i)
            {
                $data[] = array_combine($keyNames, $i);
            }

        } catch (\Exception $e) {
            $output->writeln("<fg=red>File wasn't found!\n</>");
            return Cli::RETURN_FAILURE;
        }

        foreach ($data as $item)
        {
            self::saveStore($item);
            $output->writeln("<fg=blue>-{$item['name']} imported!</>");
        }


        $output->writeln("<fg=green>\nAll stores were successfully imported!</>");

        return Cli::RETURN_SUCCESS;
    }

    private function saveStore(array $data)
    {
        $address = $data['address'] .', '. $data['city'] .', '. $data['zip'] .', '. $data['country'];


        if($data['schedule'] !== '')
        {
            $schedule = explode('-', $data['schedule']);

        }

        $data = [
            'name' => $data['name'] ?? '',
            'description' => $data['description'] ?? '',
            'image' => $data['img'] ?? '',
            'phone' => $data['phone'] ?? '',
            'url_key' => $data['url_key'] ?? '',
            'address' => $address,
            'longitude' => $data['longitude'] ?? '',
            'latitude' => $data['latitude'] ?? '',
            'schedule'=>$schedule['schedule']??''
        ];

        $store = $this->shopFactory->create();
        $store->setData($data);
        $this->shopRepository->save($store);
    }
}
