<?php

declare(strict_types=1);

namespace Elogic\StoreLocator\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;

class ShopConfig
{
    const XML_PATH_ENABLED = 'elogic_store/general/enabled';
    const XML_PATH_API_KEY = 'elogic_store/general/map_api';

    private ScopeConfigInterface $config;

    public function __construct(ScopeConfigInterface $config)
    {
        $this->config = $config;
    }

    public function isEnabled(): string
    {
        return $this->config->getValue(self::XML_PATH_ENABLED);
    }

    public function getApiKey(): string
    {
        return $this->config->getValue(self::XML_PATH_API_KEY);
    }
}
