<?php

//declare(strict_types=1);

namespace Elogic\StoreLocator\Api\Data;

interface ShopInterface
{
    const SHOP_ID = "shop_id";
    const NAME = "name";
    const DESCRIPTION = "description";
    const ADDRESS = "address";
    const IMAGE = "image";
    const SCHEDULE = "schedule";
    const LONGITUDE = "longitude";
    const LATITUDE = "latitude";
    const URL_KEY = "url_key";

    /**
     * @param $id
     * @return ShopInterface
     */
    public function getByShopId($id): ShopInterface;

    /**
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * @return string|null
     */
    public function getDescription(): ?string;

    /**
     * @return string|null
     */
    public function getAddress(): ?string;


    /**
     * @return string|null
     */
    public function getImage(): ?string;

    /**
     * @return string|null
     */
    public function getSchedule(): ?string;

    /**
     * @return float|null
     */
    public function getLongitude(): ?float;

    /**
     * @return float|null
     */
    public function getLatitude(): ?float;
    /**
     *  @return string|null
     */
    public function getUrlKey(): ?string;
    /**
     * @param int $id

     */
    public function setShopId(int $id);

    /**
     * @param string $name
     */
    public function setName(string $name);

    /**
     * @param string $address
     */
    public function setAddress(string $address);


    /**
     * @param string $image

     */
    public function setImage(string $image);

    /**
     * @param string $schedule

     */
    public function setSchedule(string $schedule);

    /**
     * @param float $longitude
     */
    public function setLongitude(float $longitude);

    /**
     * @param float $latitude

     */
    public function setLatitude(float $latitude);
    /**
     * @param string $url
     */
    public function setUrlKey(string $urlKey);
}
