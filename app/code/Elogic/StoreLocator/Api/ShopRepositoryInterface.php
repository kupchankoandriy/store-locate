<?php

declare(strict_types=1);

namespace Elogic\StoreLocator\Api;

use Elogic\StoreLocator\Api\Data\ShopInterface;

interface ShopRepositoryInterface
{

    /**
     * @param int $id
     * @return ShopInterface
     */
    public function getById(int $id) : ShopInterface;


    /**
     * @param ShopInterface $shop
     * @return ShopInterface
     */
    public function save(ShopInterface $shop) : ShopInterface;


    /**
     * @param ShopInterface $shop
     * @return void
     */
    public function delete(ShopInterface $shop) : void;

    /**
     * @param int $id
     * @return void
     */
    public function deleteById(int $id) : void;

    /**
     * @return Elogic\StoreLocator\Api\Data\ShopInterface[]
     */
    public function getList() : array;

}

