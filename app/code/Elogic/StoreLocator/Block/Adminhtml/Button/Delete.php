<?php

namespace Elogic\StoreLocator\Block\Adminhtml\Button;

use Magento\Backend\App\Action\Context;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class Delete implements ButtonProviderInterface
{
    /**
     * @var Context
     */
    private $context;
    /**
     * @var UrlInterface
     */
    private $url;

    public function __construct(Context $context, UrlInterface $url)
    {
        $this->context = $context;
        $this->url = $url;
    }
    public function getButtonData()
    {
        $data=[];
        if($this->getShopId()){
            $data = [
                'label' => __('Delete this Shop'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm(\'' . __(
                        'Are you sure, you want to delete this shop?'
                    ) . '\', \'' . $this->getDeleteUrl() . '\', {"data": {}})',
                'sort_order' => 20,
            ];
        }
        return $data;
        // TODO: Implement getButtonData() method.
    }

    private function getShopId()
    {
        return $this->context->getRequest()->getParam('shop_id');
    }

    private function getDeleteUrl(): string
    {
        return $this->url->getUrl("*/*/delete", ["shop_id" => $this->getShopId()]);
    }

}
