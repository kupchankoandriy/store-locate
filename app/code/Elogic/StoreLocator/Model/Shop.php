<?php



namespace Elogic\StoreLocator\Model;

use Elogic\StoreLocator\Api\Data\ShopInterface;
use Elogic\StoreLocator\Model\ResourceModel;
use Magento\Framework\Model\AbstractModel;


class Shop extends AbstractModel implements ShopInterface
{
    protected $_eventPrefix = "shop";
    protected $_eventObject = "shop";

    protected function _construct()
    {
        $this->_init(ResourceModel\Shop::class);
    }

    /**
     * @param int $id
     * @return ShopInterface
     */
    public function setShopId(int $id): ShopInterface
    {
        return $this->setData(self::SHOP_ID, $id);
    }


    /**
     * @param $id
     * @return ShopInterface
     */
    public function getByShopId($id) : ShopInterface
    {
        return $this->getData(self::SHOP_ID);
    }

    /**
     * @param string $name
     * @return ShopInterface
     */
    public function setName(string $name): ShopInterface
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->getData(self::NAME);
    }
    /**
     * @param string $description
     * @return ShopInterface
     */
    public function setDescription(string $description): ShopInterface
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @param string $address
     * @return ShopInterface
     */
    public function setAddress(string $address): ShopInterface
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->getData(self::ADDRESS);
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->getData(self::IMAGE);
    }

    /**
     * @param string $image
     * @return ShopInterface
     */
    public function setImage(string $image): ShopInterface
    {
        return $this->setData(self::IMAGE, $image);
    }

    /**
     * @param string $schedule
     * @return ShopInterface
     */
    public function setSchedule(string $schedule): ShopInterface
    {
        return $this->setData(self::SCHEDULE, $schedule);
    }

    /**
     * @return string|null
     */
    public function getSchedule(): ?string
    {
        return $this->getData(self::SCHEDULE);
    }

    /**
     * @param float $longitude
     * @return ShopInterface
     */
    public function setLongitude(float $longitude): ShopInterface
    {
        return $this->setData(self::LONGITUDE, $longitude);
    }

    /**
     * @return float|null
     */
    public function getLongitude(): ?float
    {
        (float)$result  = $this->getData(self::LONGITUDE);
         return $result;
    }

    /**
     * @param float $latitude
     * @return ShopInterface
     */
    public function setLatitude(float $latitude): ShopInterface
    {
        return $this->setData(self::LATITUDE, $latitude);
    }

    /**
     * @return float|null
     */
    public function getLatitude(): ?float
    {
        (float) $result  = $this->getData(self::LATITUDE);

        return $result;
    }

    public function getUrlKey(): ?string
    {
        return parent::getData(self::URL_KEY);
    }

    public function setUrlKey(string $urlKey) :ShopInterface
    {
        return $this->setData(self::URL_KEY, $urlKey);
    }
}
