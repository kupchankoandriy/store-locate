<?php

declare(strict_types=1);

namespace Elogic\StoreLocator\Model;

use Elogic\StoreLocator\Api\ShopRepositoryInterface;
use Elogic\StoreLocator\Api\Data\ShopInterface;
use Elogic\StoreLocator\Model\ResourceModel\Shop\CollectionFactory;
use Elogic\StoreLocator\Model\ResourceModel\ShopFactory as ShopResourceFactory;
use Magento\Framework\Exception\NoSuchEntityException;

class ShopRepository implements ShopRepositoryInterface
{

    /**
     * @var ShopResourceFactory
     */
    private ShopResourceFactory $shopResourceFactory;
    /**
     * @var  ShopFactory
     */
    private ShopFactory $shopFactory;

    /**
     * @var  CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @param ShopFactory $shopFactory
     * @param ShopResourceFactory $shopResourceFactory
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        ShopFactory $shopFactory,
        ShopResourceFactory $shopResourceFactory,
        CollectionFactory  $collectionFactory

    )
    {
        $this->shopFactory = $shopFactory;
        $this->shopResourceFactory = $shopResourceFactory;
        $this->collectionFactory = $collectionFactory;
    }


    /**
     * @param int $id
     * @return \Elogic\StoreLocator\Api\Data\ShopInterface
     * @throws NoSuchEntityException
     */
    public function getById(int $id) : ShopInterface
    {
        $shop = $this->shopFactory->create();
        $this->shopResourceFactory->create()->load($shop, $id);
        if (!$shop->getId()) {
            throw new NoSuchEntityException(__("There is no shop with this id"));
        }
        return $shop;
    }


    /**
     * @param ShopInterface $shop
     * @return ShopInterface
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save(ShopInterface $shop) : ShopInterface
    {
        $this->shopResourceFactory->create()->save($shop);
        return $shop;
    }


    /**
     * @param $shop
     * @return void
     * @throws \Exception
     */
    public function delete($shop): void
    {
        $this->shopResourceFactory->create()->delete($shop);
    }

    /**
     * @param int $id
     * @return void
     * @throws \Exception
     */
    public function deleteById(int $id): void
    {
       $shop = $this->getById($id);
       $this->delete($shop);
    }

    /**
     * @return Elogic\StorLocator\Model\Shop[]
     */
    public function getList(): array
    {
        return $this->collectionFactory->create()->getItems();
    }

    public function getByUrlKey(string $urlKey): ?StoreInterface
    {
        try {
            $store = $this->collectionFactory->create()->getItemByColumnValue(self::URL_KEY ,$urlKey);
        } catch (\Exception $e) {
            return false;
        }
        return $store;
    }


}
