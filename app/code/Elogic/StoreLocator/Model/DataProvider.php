<?php
declare(strict_types=1);

namespace Elogic\StoreLocator\Model;

use Elogic\StoreLocator\Api\ShopRepositoryInterface;
use Elogic\StoreLocator\Model\ResourceModel\Shop\CollectionFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Ui\DataProvider\ModifierPoolDataProvider;

class DataProvider extends ModifierPoolDataProvider
{
    /**
     * @var RequestInterface
     */
    private RequestInterface $request;


    /**
     * @var ShopRepositoryInterface
     */
    private ShopRepositoryInterface $shopRepository;
    /**
     * @var ShopFactory Factory
     */
    private ShopFactory $shopFactory;
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;
    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param array $meta
     * @param array $data
     * @param null $pool
     * @param RequestInterface $request
     * @param ShopRepositoryInterface $shopRepository
     * @param ShopFactory $shopFactory
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = [],
        $pool = null,
        RequestInterface $request,
        ShopRepositoryInterface $shopRepository,
        ShopFactory $shopFactory,
        CollectionFactory $collectionFactory
    )
    {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
        $this->request = $request;
        $this->shopRepository = $shopRepository;
        $this->shopFactory = $shopFactory;
        $this->collectionFactory = $collectionFactory;
        $this->collection = $collectionFactory->create();
    }

    public function getData(): array
    {

        $shopId = $this->request->getParam($this->requestFieldName);
        try {
            $shop = $this->shopRepository->getById((int)$shopId);
        } catch (NoSuchEntityException $exception) {
            $shop = $this->shopFactory->create();
        }
        $this->loadedData[$shop->getId()] = $shop->getData();
        return $this->loadedData;
    }
}

