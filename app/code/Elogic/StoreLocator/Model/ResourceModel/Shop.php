<?php

namespace Elogic\StoreLocator\Model\ResourceModel;

class Shop extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb

{
    protected function _construct()
    {
        $this->_init('shops', 'shop_id');
    }


}
