<?php

namespace Elogic\StoreLocator\Model\ResourceModel\Shop;

use Elogic\StoreLocator\Model\Shop;
use Elogic\StoreLocator\Model\ResourceModel\Shop as ShopResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends  AbstractCollection
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init(
            Shop::class,
            ShopResource::class
        );
    }
}
