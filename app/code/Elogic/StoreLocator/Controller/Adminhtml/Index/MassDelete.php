<?php

namespace Elogic\StoreLocator\Controller\Adminhtml\Index;

use Elogic\StoreLocator\Api\ShopRepositoryInterface;
use Elogic\StoreLocator\Model\ResourceModel\Shop\CollectionFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;



/**
 * Class MassDelete
 */
class MassDelete extends Action implements HttpPostActionInterface
{
    /**
     * @var Filter
     */
    protected Filter $filter;

    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $collectionFactory;
    /**
     * @var ShopRepositoryInterface
     */
    private ShopRepositoryInterface $shopRepository;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param ShopRepositoryInterface $shopRepository
     */
    public function __construct(
        Context                 $context,
        Filter                  $filter,
        CollectionFactory       $collectionFactory,
        ShopRepositoryInterface $shopRepository
    )
    {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->shopRepository = $shopRepository;
        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $collection = $this->collectionFactory->create();
        $ids = $this->getRequest()->getParam("selected");
        if ($ids) {
            $collection->addFieldToFilter("shop_id", ["in"=>$ids]);
        }
        $categoryDeleted = 0;

        if ($ids || $this->getRequest()->getParam('excluded') === "false"){
            foreach ($collection->getItems() as $shop) {
                $this->shopRepository->delete($shop);
                $categoryDeleted++;
            }
        }

        if ($categoryDeleted) {
            $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been deleted.', $categoryDeleted));
        }

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('elogic_storelocator/index/index');
    }

}
