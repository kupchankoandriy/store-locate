<?php
namespace Elogic\StoreLocator\Controller\Adminhtml\Index;

use Elogic\StoreLocator\Api\ShopRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class Delete extends Action implements HttpPostActionInterface
{
    private ShopRepositoryInterface $shopRepository;

    public function __construct(Context $context, ShopRepositoryInterface $shopRepository)
    {
        parent::__construct($context);
        $this->shopRepository = $shopRepository;
    }
    public function execute()
    {
       $shopId = $this->getRequest()->getParam('shop_id');
       if(!$shopId){
           $this->messageManager->addErrorMessage("No author Specified");
       }
       try{
           $this->shopRepository->deleteById($shopId);
           $this->messageManager->addSuccessMessage("Shop has been deleted");
       }
       catch (NoSuchEntityException $exception){
           $this->messageManager->addErrorMessage("Can't find such shop to delete it");
       }

        $redirect = $this->resultRedirectFactory->create();
        return $redirect->setPath("*/*/index");
        // TODO: Implement execute() method.
    }
}
