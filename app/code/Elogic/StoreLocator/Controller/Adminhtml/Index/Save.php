<?php

namespace Elogic\StoreLocator\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Elogic\StoreLocator\Api\ShopRepositoryInterface;
use Elogic\StoreLocator\Model\ShopFactory;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;

class Save extends Action implements HttpGetActionInterface, HttpPostActionInterface
{
    /**
     * @var ShopRepositoryInterface
     */
    private ShopRepositoryInterface $shopRepository;

    /**
     * @var ShopFactory
     */
    private ShopFactory $shopFactory;

    /**
     * @var RedirectFactory
     */
    private RedirectFactory $redirectFactory;

    /**
     * @param Context $context
     * @param ShopRepositoryInterface $shopRepository
     * @param ShopFactory $shopFactory
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Context $context,
        ShopRepositoryInterface $shopRepository,
        ShopFactory $shopFactory,
        RedirectFactory $redirectFactory)
    {
        parent::__construct($context);
        $this->shopRepository = $shopRepository;
        $this->shopFactory = $shopFactory;
        $this->redirectFactory = $redirectFactory;
    }

    public function execute()
    {
        $shopData = $this->getRequest()->getParams();

        if (empty($shopData['shop_id'])) {
            $shopData['shop_id'] = null;
        }
        $shop = $this->shopFactory->create();
        $shop->setData($shopData);
        $this->shopRepository->save($shop);


       return $this->redirectFactory->create()->setPath("*/*/index");

    }

}
