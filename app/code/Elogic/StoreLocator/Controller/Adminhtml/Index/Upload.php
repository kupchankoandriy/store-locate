<?php

namespace Elogic\StoreLocator\Controller\Adminhtml\Index;

use function Elogic\StoreLocator\Controller\Adminhtml\Images\__;

class Upload extends \Magento\Backend\App\Action {

    /**
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magento_Catalog::catalog_products');
       // $resultPage->setActiveMenu('VENDOR_ImageUploader::images_uploader');
        $resultPage->getConfig()->getTitle()->prepend(__('Upload Image'));
        return $resultPage;
    }
}
