<?php

declare(strict_types=1);

namespace Elogic\StoreLocator\Api\Data;

interface ShopInterface
{
    const SHOP_ID = "shop_id";
    const NAME = "name";
    const DESCRIPTION = "description";
    const ADDRESS = "address";
    const IMAGE = "image";
    const SCHEDULE = "schedule";
    const LONGITUDE = "longitude";
    const LATITUDE =  "latitude";


    /**
     * @return ShopInterface
     */
    public function getByShopId($id) : ShopInterface;

    /**
     * @return string|null
     */
    public function getName() : ?string;

    /**
     * @return string|null
     */
    public function getDescription() : ?string;

    /**
     * @return string|null
     */
    public function getAddress() : ?string;


    /**
     * @return string|null
     */
    public function getImage() : ?string;

    /**
     * @return string|null
     */
    public function getSchedule() : ?string;

    /**
     * @return float|null
     */
    public function getLongitude() : ?float;

    /**
     * @return float|null
     */
    public function getLatitude() : ?float;

    /**
     * @param int $id
     * @return ShopInterface
     */
    public function setShopId(int $id) : ShopInterface;

    /**
     * @param string $name
     * @return ShopInterface
     */
    public function setName(string $name) : ShopInterface;

    /**
     * @param string $address
     * @return ShopInterface
     */
    public function setAddress(string $address) : ShopInterface;


    /**
     * @param string $image
     * @return ShopInterface
     */
    public function setImage(string $image) : ShopInterface;

    /**
     * @param float $schedule
     * @return ShopInterface
     */
    public function setSchedule(string $schedule) : ShopInterface;

    /**
     * @param float $longitude
     * @return ShopInterface
     */
    public function setLongitude(float $longitude) : ShopInterface;

    /**
     * @param float $latitude
     * @return ShopInterface
     */
    public function setLatitude(float $latitude) : ShopInterface;
}
